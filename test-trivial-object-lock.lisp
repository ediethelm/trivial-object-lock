(in-package :trivial-object-lock)

(5am:def-suite :trivial-object-lock-tests :description "Trivial Object Lock tests")
(5am:in-suite :trivial-object-lock-tests)

(uiop:define-package #:trivial-object-lock-test
  (:use #:cl)
  (:export #:locked-object))

(in-package :trivial-object-lock-test)
(defparameter locked-object (make-hash-table))

(5am:test with-object-lock-held/external
  (5am:finishes
    (trivial-object-lock:with-object-lock-held (locked-object :test #'eql)
      (+ 1 2)))

  (5am:finishes
    (trivial-object-lock:with-object-lock-held (locked-object :test #'eql)
      (+ 1 2))))

(in-package :trivial-object-lock)

(5am:test acquire-release
  (let ((lock1 (gensym)))
    (5am:finishes (acquire-lock lock1 nil))
    (5am:finishes (release-lock lock1 nil))
    (5am:finishes (acquire-lock lock1 nil))
    (5am:finishes (release-lock lock1 nil))))

(5am:test with-object-lock-held
  (5am:finishes
    (with-object-lock-held (trivial-object-lock-test:locked-object :test #'equal)
      (setf (gethash 2 trivial-object-lock-test:locked-object) '(:a))))

  (5am:finishes
    (with-object-lock-held (trivial-object-lock-test:locked-object :test #'equal)
      (setf (gethash 1  trivial-object-lock-test:locked-object) '(:b)))))

(5am:test with-object-lock-held/threaded
  (let ((result nil)
	(expected '((F E C D B A) (F E D C B A)))
	(object (gensym))
	(prop1 (gensym))
	(prop2 (gensym)))

    (acquire-lock object prop1)

    (bordeaux-threads:make-thread #'(lambda () (with-object-lock-held (object :property prop2) (push 'A result) (sleep 1))))
    (sleep 0.1)
    (bordeaux-threads:make-thread #'(lambda () (with-object-lock-held (object) (push 'B result) (sleep 1))))
    (sleep 0.1)
    (bordeaux-threads:make-thread #'(lambda () (with-object-lock-held (object :property prop1) (push 'C result) (sleep 1))))
    (sleep 0.1)
    (bordeaux-threads:make-thread #'(lambda () (with-object-lock-held (object :property prop2) (push 'D result) (sleep 1))))
    (sleep 0.1)
    (bordeaux-threads:make-thread #'(lambda () (with-object-lock-held (object :property prop2) (push 'E result) (sleep 1))))
    (sleep 0.1)
    (bordeaux-threads:make-thread #'(lambda () (with-object-lock-held (object) (push 'F result) (sleep 1))))

    (sleep 2)

    (release-lock object prop1)
    (sleep 8)

    (5am:is (member result expected :test #'equalp))))

(5am:test recursive-lock
  (let ((object (gensym))
	(prop1 (gensym))
	(res nil))

    (bordeaux-threads:make-thread #'(lambda ()
				      (acquire-lock object nil)
				      (with-object-lock-held
					  (object :property prop1)
					(setf res 42))
				      (release-lock object nil)))

    (sleep 2)
    (5am:is (eq res 42))))
